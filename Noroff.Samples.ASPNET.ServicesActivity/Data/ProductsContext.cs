﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Reflection.Emit;

namespace Noroff.Samples.ASPNET.ServicesActivity.Data
{
    public class ProductsContext : DbContext
    {
        public ProductsContext(DbContextOptions<ProductsContext> options)
            : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }

        // Additional DbSets for other entities can be added here

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Seed data for the Product table
            modelBuilder.Entity<Product>().HasData(
                new Product(1, "Laptop", "High-performance laptop", 1200.00M),
                new Product(2, "Smartphone", "Latest model smartphone", 800.00M),
                new Product(3, "Headphones", "Noise-cancelling headphones", 200.00M)
            );
        }

    }
}
