﻿namespace Noroff.Samples.ASPNET.ServicesActivity.Data
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        // Parameterless constructor for EF
        public Product() { }

        // Constructor with parameters for easy seeding
        public Product(int id, string name, string description, decimal price)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
        }
    }
}
