﻿using Noroff.Samples.ASPNET.ServicesActivity.Data;

namespace Noroff.Samples.ASPNET.ServicesActivity.Repositories
{
    public interface IProductRepository
    {
        Task<IEnumerable<Product>> GetAllAsync();
        Task<Product> GetByIdAsync(int id);
        Task AddAsync(Product product);
        Task UpdateAsync(Product product);
        Task DeleteAsync(int id);
    }
}
