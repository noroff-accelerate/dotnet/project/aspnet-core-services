using Microsoft.EntityFrameworkCore;
using Noroff.Samples.ASPNET.ServicesActivity.Data;
using Noroff.Samples.ASPNET.ServicesActivity.Repositories;
using Noroff.Samples.ASPNET.ServicesActivity.Services;

namespace Noroff.Samples.ASPNET.ServicesActivity
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.
            builder.Services.AddDbContext<ProductsContext>(opt => 
                opt.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
            builder.Services.AddScoped<IProductRepository, ProductRepository>();
            builder.Services.AddScoped<IProductService, ProductService>();
            builder.Services.AddControllers();

            var app = builder.Build();

            // Configure the HTTP request pipeline.

            app.UseHttpsRedirection();

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}