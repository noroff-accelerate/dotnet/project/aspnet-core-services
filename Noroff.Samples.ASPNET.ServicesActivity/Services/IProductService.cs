﻿using Noroff.Samples.ASPNET.ServicesActivity.Data;

namespace Noroff.Samples.ASPNET.ServicesActivity.Services
{
    public interface IProductService
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
        Task<Product> GetProductByIdAsync(int id);
        Task AddProductAsync(Product product);
        Task UpdateProductAsync(Product product);
        Task DeleteProductAsync(int id);
    }
}
