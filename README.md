# ASP.NET Core Web API with Entity Framework Core

## Overview
This ASP.NET Core Web API project is integrated with Entity Framework Core and demonstrates CRUD operations on a product catalog. It includes a repository and service layer to illustrate a clean architecture and the effective use of Dependency Injection.

## Features
- Entity Framework Core integration with SQL Server.
- Dependency Injection for managing services and repositories.
- CRUD operations for a product catalog.
- Pre-seeded data for initial testing.

## Prerequisites
- .NET 6.0 SDK or later.
- Visual Studio 2019 or later with ASP.NET and web development workload.
- SQL Server (LocalDB, Express, or other editions).

1. **Clone the Repository**: Clone this repository to your local machine or download the source code.

2. **Open the Project**: Open the solution file in Visual Studio.

3. **Restore NuGet Packages**: Ensure all NuGet packages are restored correctly.

4. **Apply Migrations**: Use the Package Manager Console in Visual Studio to apply migrations to your SQL Server database:
     ```
     Update-Database
     ```
   Make sure to update the connection string in `appsettings.json` as needed.

5. **Run the Application**: Build and run the application in Visual Studio using IIS Express or Kestrel.

## Testing the API
The API is pre-seeded with sample product data. Use tools like Postman or Swagger to interact with the API.

- `GET /api/products`: Retrieves all products.
- `GET /api/products/{id}`: Retrieves a product by its ID.
- `POST /api/products`: Adds a new product.
- `PUT /api/products/{id}`: Updates an existing product.
- `DELETE /api/products/{id}`: Deletes a product by its ID.

## Seeded Data
The database is seeded with the following products:
- Laptop: High-performance laptop priced at $1200.00.
- Smartphone: Latest model smartphone priced at $800.00.
- Headphones: Noise-cancelling headphones priced at $200.00.

## Contributing
Contributions are welcome. Feel free to fork the repository, make changes, and submit a pull request. For substantial changes, please open an issue first to discuss your ideas.

## License
This project is licensed under the [MIT License](LICENSE).
